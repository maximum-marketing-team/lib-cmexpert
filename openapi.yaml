openapi: 3.0.0
info:
  title: 'Appraisal API: Калькулятор оценки стоимости авто с пробегом'
  version: 1.1.0
  description: |+

    # Appraisal API
    **Appraisal API** — API сервиса по оценке стоимости авто с пробегом
servers:
  - description: Appraisal API
    url: https://appraisal.api.cm.expert/v1
  - description: Custom URL
    url: '{url}'
    variables:
      url:
        default: https://appraisal.api.cm.expert/v1
paths:
  /autocatalog/brands:
    get:
      tags:
        - Автокаталог
      summary: Список марок
      description: Получение списка марок легковых автомобилей
      operationId: getBrands
      responses:
        '200':
          description: Список марок легковых автомобилей
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BrandsResponse'
  /autocatalog/models:
    get:
      tags:
        - Автокаталог
      summary: Список моделей
      operationId: getModels
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: true
          schema:
            type: integer
            example: 127
      responses:
        '200':
          description: Список моделей
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ModelsResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for ModelsRequestModel
                  brand
                    field required (type=value_error.missing)
  /autocatalog/creationYears:
    get:
      tags:
        - Автокаталог
      summary: Года выпуска производства всех модификаций, выбранной модели
      operationId: getCreationYears
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: true
          schema:
            type: integer
            example: 127
        - name: model
          in: query
          description: Идентификатор модели
          required: true
          schema:
            type: integer
            example: 3169
      responses:
        '200':
          description: Года выпуска модели
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CreationYearsResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for CreationYearsRequestModel
                  brand
                    field required (type=value_error.missing)
  /autocatalog/generations:
    get:
      tags:
        - Автокаталог
      summary: Все поколения выбранной модели, находящиеся в производстве в выбранный год
      operationId: getGenerations
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: true
          schema:
            type: integer
            example: 127
        - name: model
          in: query
          description: Идентификатор модели
          required: true
          schema:
            type: integer
            example: 3169
        - name: creationYear
          in: query
          description: Год выпуска
          required: false
          schema:
            type: integer
            example: 2008
      responses:
        '200':
          description: Поколения
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GenerationsResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for GenerationsRequestModel
                  brand
                    field required (type=value_error.missing)
  /autocatalog/bodies:
    get:
      tags:
        - Автокаталог
      summary: Все кузова доступные для выбранной модели, года и поколения
      description: >-
        Если указаны brand или model, то значения будут выбраны из автокаталога,
        иначе будут возвращены все возможные значения для свойства кузов
        без указания количества дверей.
      operationId: getBodies
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: false
          schema:
            type: integer
            example: 127
        - name: model
          in: query
          description: Идентификатор модели
          required: false
          schema:
            type: integer
            example: 3169
        - name: creationYear
          in: query
          description: Год выпуска
          required: false
          schema:
            type: integer
            example: 2008
        - name: generation
          in: query
          description: Идентификатор поколения
          required: false
          schema:
            type: integer
            example: 2736
      responses:
        '200':
          description: Список кузовов с дверьми
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BodiesResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for BodiesRequestModel
                  brand
                    value is not a valid integer (type=type_error.integer)
  /autocatalog/gears:
    get:
      tags:
        - Автокаталог
      summary: Все типы КПП, доступные для выбранных модели, года, поколения, и кузова
      description: >-
        Если указаны brand или model, то значения будут выбраны из автокаталога,
        иначе будут возвращены все возможные значения для свойства КПП.
      operationId: getGears
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: false
          schema:
            type: integer
            example: 127
        - name: model
          in: query
          description: Идентификатор модели
          required: false
          schema:
            type: integer
            example: 3169
        - name: creationYear
          in: query
          description: Год выпуска
          required: false
          schema:
            type: integer
            example: 2008
        - name: generation
          in: query
          description: Идентификатор поколения
          required: false
          schema:
            type: integer
            example: 2736
        - name: body
          in: query
          description: Идентификатор кузова
          required: false
          schema:
            type: integer
            example: 13
        - name: doors
          in: query
          description: Количество дверей
          required: false
          schema:
            type: integer
            example: 4
      responses:
        '200':
          description: Список КПП
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GearsResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for GearsRequestModel
                  brand
                    value is not a valid integer (type=type_error.integer)
  /autocatalog/drives:
    get:
      tags:
        - Автокаталог
      summary: >-
        Все типы приводов, доступные для выбранных модели, года выпуска,
        поколения, кузова и КПП
      description: >-
        Если указаны brand или model, то значения будут выбраны из автокаталога,
        иначе будут возвращены все возможные значения для свойства Тип привода.
      operationId: getDrives
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: false
          schema:
            type: integer
            example: 127
        - name: model
          in: query
          description: Идентификатор модели
          required: false
          schema:
            type: integer
            example: 3169
        - name: creationYear
          in: query
          description: Год выпуска
          required: false
          schema:
            type: integer
            example: 2008
        - name: generation
          in: query
          description: Идентификатор поколения
          required: false
          schema:
            type: integer
            example: 2736
        - name: body
          in: query
          description: Идентификатор кузова
          required: false
          schema:
            type: integer
            example: 13
        - name: doors
          in: query
          description: Количество дверей
          required: false
          schema:
            type: integer
            example: 4
        - name: gear
          in: query
          description: Идентификатор типа КПП
          required: false
          schema:
            type: integer
            example: 2
      responses:
        '200':
          description: Список приводов
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DrivesResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for DrivesRequestModel
                  brand
                    value is not a valid integer (type=type_error.integer)
  /autocatalog/engines:
    get:
      tags:
        - Автокаталог
      summary: >-
        Все двигатели (тип, объем и мощность), доступные для выбранных модели,
        года выпуска, поколения, кузова, КПП и типа привода
      description: >-
        Если указаны brand или model, то значения будут выбраны из автокаталога,
        иначе будут возвращены все возможные значения для свойства Тип двигателя
        без указания мощности и объема двигателя.
      operationId: getEngines
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: false
          schema:
            type: integer
            example: 127
        - name: model
          in: query
          description: Идентификатор модели
          required: false
          schema:
            type: integer
            example: 3169
        - name: creationYear
          in: query
          description: Год выпуска
          required: false
          schema:
            type: integer
            example: 2008
        - name: generation
          in: query
          description: Идентификатор поколения
          required: false
          schema:
            type: integer
            example: 2736
        - name: body
          in: query
          description: Идентификатор кузова
          required: false
          schema:
            type: integer
            example: 13
        - name: doors
          in: query
          description: Количество дверей
          required: false
          schema:
            type: integer
            example: 4
        - name: gear
          in: query
          description: Идентификатор типа КПП
          required: false
          schema:
            type: integer
            example: 2
      responses:
        '200':
          description: Список двигателей
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EnginesResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for EnginesRequestModel
                  brand
                    value is not a valid integer (type=type_error.integer)
  /autocatalog/modifications:
    get:
      tags:
        - Автокаталог
      summary: Получение списка модификации
      operationId: getModifications
      parameters:
        - name: brand
          in: query
          description: Идентификатор марки
          required: true
          schema:
            type: integer
            example: 127
        - name: model
          in: query
          description: Идентификатор модели
          required: true
          schema:
            type: integer
            example: 3169
        - name: creationYear
          in: query
          description: Год выпуска
          required: true
          schema:
            type: integer
            example: 2008
        - name: generation
          in: query
          description: Идентификатор поколения
          schema:
            type: integer
            example: 2736
      responses:
        '200':
          description: Список модификаций
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ModificationsResponse'
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: |-
                  1 validation error for ModificationsRequestModel
                  creationYear
                    field required (type=value_error.missing)


  /engineTypes:
    get:
      tags:
        - Автокаталог
      summary: Список допустимых значений типов двигателей
      deprecated: true
      description: Метод устарел. Используйте /autocatalog/drives без параметров.
      operationId: getEngineTypes
      responses:
        '200':
          description: Список допустимых значений типов двигателей
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EngineTypesResponse'
  /regions:
    get:
      tags:
        - Оценка
      summary: Список регионов
      operationId: getRegions
      responses:
        '200':
          description: Список регионов
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RegionsResponse'
  /modification:
    get:
      tags:
        - Оценка
      summary: Получение модификации по вину
      operationId: getModification
      parameters:
        - name: vin
          in: query
          description: VIN автомобиля
          required: true
          schema:
            type: string
            format: VIN
      responses:
        '200':
          description: Расшифрованная модификация по VIN
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ModificationByVinResponse'
        '404':
          description: Невозможно декодировать вин
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /appraisal:
    get:
      tags:
        - Оценка
      summary: Выполнение оценки легкового автомобиля
      operationId: getAppraisal
      parameters:
        - name: vin
          in: query
          description: >-
            Модификация авто для оценки будет получена расшифровкой VIN.
            Не все параметры модификации могут быть распознаны по VIN.
            Для уточненной модификации стоит использовать параметры модификации.
            Обязательный параметр, но взаимоисключаем модификацией.
          required: false
          schema:
            type: string
            format: VIN
            example: WVWCB5155NK026829
        - name: modification
          in: query
          description: >-
            Уточненная модификация. Обязательный параметр, но взаимоисключаем VIN.
          required: false
          style: form
          explode: true
          schema:
            $ref: '#/components/schemas/ModificationRequest'
          example:
            brand: 127
            model: 3169
            creationYear: 2008
            body: 13
            doors: 4
            gear: 2
            drive: 1
            engine: 1
            volume: 1.4
            power: 75
            wheel: 1
            generation: 2736
        - name: regionId
          in: query
          description: >-
            Регион/регионы заданные идентификаторами регионов, которые будут
            участвовать в подборе похожих авто, для расчета диапазона цен и среднего пробега.
            Обязательный параметр, но взаимоисключаемы географическими координатами.
          style: form
          explode: true
          schema:
            type: array
            items:
              type: integer
            example: [ 1960671, 1573301 ]
        - name: geo
          in: query
          description: >-
            Регион заданный географическими координатами и радиусом, который будет
            участвовать в подборе похожих авто, для расчета диапазона цен и среднего пробега.
            Обязательный параметр, но взаимоисключаем идентификатороми регионов.
          style: form
          explode: true
          schema:
            $ref: '#/components/schemas/GeoRequest'
          example:
            lat: 55.753215
            lon: 37.622504
            radius: 100
            maxExtendingRadius: 500
        - name: mileage
          in: query
          description: Пробег в км
          required: false
          schema:
            type: integer
            example: 20000
        - name: similarThreshold
          in: query
          description: Целевое количество похожих предложений для оценки. Должно быть не меньше 10.
          required: false
          schema:
            type: integer
            default: 10
            example: 10
        - name: liquidityRegionId
          in: query
          description: >-
            Идентификатор региона, в котором будет производиться оценка ликвидности.
            Если параметр осутствует, а регион поиска похожих авто задан одним
            идентификатором региона, то будет использован он в качестве региона оценки ликвидности.
          required: false
          schema:
            type: integer
            example: 1960671
      responses:
        '200':
          description: Оценка по входным параметрам
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AppraisalResponse'
              example:
                priceCategories:
                  low:
                    min: 108926.94611472932
                    max: 150877.29897813383
                  middle:
                    min: 150877.29897813383
                    max: 192827.65184153832
                  high:
                    min: 192827.65184153832
                    max: 234778.0047049428
                correctedPriceCategories:
                  low:
                    min: 145853.71558699588
                    max: 184746.57186233197
                  middle:
                    min: 184746.57186233197
                    max: 223639.42813766803
                  high:
                    min: 223639.42813766803
                    max: 262532.2844130041
                similarCarsAvgMileage: 190845.19672131148
                complexEstimate: 5
                complexMdsDays: 19
                removedParams: [ 'hello', 'world' ]
                similarCars: 61
                correctedAveragePriceMileage: 195168.87101526989
        '400':
          description: Ошибка валидации входных параметров.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
              example:
                error: geo or regionId is wrong
        '404':
          description: Невозможно декодировать вин, в случае оценки по вину
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '422':
          description: Сликом мало данных для выполнения оценки
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
  schemas:
    BrandsResponse:
      type: object
      required:
        - brands
      properties:
        brands:
          type: array
          title: Список марок автомобилей
          items:
            $ref: '#/components/schemas/Brand'
    Brand:
      type: object
      title: Марка автомобиля
      properties:
        id:
          type: integer
          example: 127
        text:
          type: string
          example: Renault
    ModelsResponse:
      type: object
      properties:
        models:
          type: array
          title: Список моделей
          items:
            $ref: '#/components/schemas/Model'
      required:
        - models
    Model:
      type: object
      title: Модель
      properties:
        id:
          type: integer
          example: 3169
        text:
          type: string
          example: Logan
    CreationYearsResponse:
      type: object
      properties:
        years:
          type: array
          title: Года выпуска
          items:
            type: integer
            example:
              - 2008
              - 2007
      required:
        - years
    GenerationsResponse:
      type: object
      properties:
        generations:
          type: array
          title: Поколения
          items:
            $ref: '#/components/schemas/Generation'
      required:
        - generations
    Generation:
      type: object
      title: Поколение
      properties:
        id:
          type: integer
          example: 2736
        text:
          type: string
          example: 1 поколение
        from:
          type: integer
          example: 2004
        to:
          type: integer
          nullable: true
          example: 2009
    BodiesResponse:
      type: object
      properties:
        bodies:
          type: array
          title: Кузова и количества дверей
          items:
            $ref: '#/components/schemas/Body'
    Body:
      type: object
      title: Кузов и количество дверей
      properties:
        id:
          type: integer
          example: 13
        text:
          type: string
          example: Седан
        doors:
          type: integer
          nullable: true
          example: 4
    GearsResponse:
      type: object
      properties:
        gears:
          type: array
          title: Список КПП
          items:
            $ref: '#/components/schemas/Gear'
    Gear:
      type: object
      title: КПП
      properties:
        id:
          type: integer
          example: 2
        text:
          type: string
          example: Механика
    DrivesResponse:
      type: object
      properties:
        drives:
          type: array
          title: Список приводов
          items:
            $ref: '#/components/schemas/Drive'
    Drive:
      type: object
      title: Привод
      properties:
        id:
          type: integer
          example: 1
        text:
          type: string
          example: Передний
    EnginesResponse:
      type: object
      properties:
        engines:
          type: array
          title: Список двигателей
          items:
            $ref: '#/components/schemas/Engine'
    Engine:
      type: object
      title: Двигатель
      properties:
        id:
          type: integer
          example: 1
        text:
          type: string
          example: Бензин
        power:
          type: integer
          nullable: true
          example: 75
        volume:
          type: number
          nullable: true
          example: 1.4
    RegionsResponse:
      type: array
      items:
        $ref: '#/components/schemas/Region'
    Region:
      type: object
      title: Region
      properties:
        regionId:
          type: integer
          example: 1960671
        name:
          type: string
          example: Москва
        typeFullName:
          type: string
          example: Город
      required:
        - regionId
        - name
        - typeFullName
    EngineTypesResponse:
      type: object
      properties:
        engineTypes:
          type: array
          title: Список типов двигателей
          items:
            $ref: '#/components/schemas/EngineType'
    EngineType:
      type: object
      title: Тип двигателя
      properties:
        id:
          type: integer
        text:
          type: string
    PriceCategories:
      type: object
      title: PriceCategories
      properties:
        low:
          $ref: '#/components/schemas/PriceCategory'
        middle:
          $ref: '#/components/schemas/PriceCategory'
        high:
          $ref: '#/components/schemas/PriceCategory'
      required:
        - low
        - middle
        - high
    PriceCategory:
      type: object
      title: Категория цен
      properties:
        min:
          type: number
          format: float
        max:
          type: number
          format: float
      required:
        - min
        - max
    Wheel:
      type: object
      title: Идентификатор руля
      description: 1 -- Левый, 2 -- Правый
      properties:
        id:
          type: integer
          example: 1
        text:
          type: string
          example: Левый
    AppraisalResponse:
      type: object
      properties:
        priceCategories:
          $ref: '#/components/schemas/PriceCategories'
        correctedPriceCategories:
          $ref: '#/components/schemas/PriceCategories'
        similarCarsAvgMileage:
          type: number
          title: Средний пробег
        complexEstimate:
          type: integer
          title: Скоринговая оценка ликвидности
        complexMdsDays:
          type: number
          title: Средняя скорость продажи в днях
        similarCars:
          type: integer
          title: Количество похожих авто
        correctedAveragePriceMileage:
          type: number
          title: Cкорректированная средняя цена по пробегу для конкретного авто.
          description: Считается в зависимости от пробега авто. Если пробег авто не передан или не хватает данных по рынку, то значение будет null
        removedParams:
          type: array
          title: Параметры, которые не учитываются в расчете
    GeoRequest:
      title: Географические координаты
      type: object
      properties:
        lat:
          description: Широта в градусах
          type: number
          format: float
        lon:
          description: Долгота в градусах
          type: number
          format: float
        radius:
          description: Радиус подбора похожих авто для оценки
          type: number
          format: integer
        maxExtendingRadius:
          description: Максимальный радиус авторасширения
          type: number
          format: integer
    ModificationsResponse:
      title: Ответ со списком модификаций
      type: object
      properties:
        modifications:
          type: array
          title: Список модификаций
          items:
            $ref: '#/components/schemas/Modification'
    Modification:
      type: object
      properties:
        brand:
          $ref: '#/components/schemas/Brand'
        model:
          $ref: '#/components/schemas/Model'
        years:
          $ref: '#/components/schemas/ProductionYears'
        generation: # maybe include years and replace with $ref: '#/components/schemas/Generation'
          $ref: '#/components/schemas/GenerationShort'
        body: # maybe include doors and replace with $ref: '#/components/schemas/Body'
          $ref: '#/components/schemas/BodyShort'
        doors:
          type: integer
          title: Количество дверей
          example: 4
        gear:
          $ref: "#/components/schemas/Gear"
        drive:
          $ref: "#/components/schemas/Drive"
        engine: # maybe include power and volume and replace with $ref: '#/components/schemas/Engine'
          $ref: '#/components/schemas/EngineShort'
        power:
          type: integer
          title: Мощность двигателя в л.с.
          example: 75
        volume:
          type: number
          nullable: true
          title: Объем двигателя в литрах или null для определенных типов двигателей
          example: 1.4
        wheel:
          $ref: "#/components/schemas/Wheel"
      required:
        - brand
        - model
        - years
        - generation
        - body
        - doors
        - gear
        - drive
        - engine
        - volume
        - power
        - wheel
    ProductionYears:
      type: object
      title: Года производства модификации
      properties:
        from:
          type: integer
          example: 2004
        to:
          type: integer
          example: 2009
    GenerationShort:
      type: object
      title: Поколение
      properties:
        id:
          type: integer
          example: 2736
        text:
          type: string
          example: 1 поколение
    BodyShort:
      type: object
      title: Тип кузова
      properties:
        id:
          type: integer
          example: 13
        text:
          type: string
          example: Седан
    EngineShort:
      type: object
      title: Двигатель
      properties:
        id:
          type: integer
          example: 1
        text:
          type: string
          example: Бензин
    ErrorResponse:
      title: Ответ в случае ошибки
      type: object
      properties:
        error:
          type: string
      required:
        - error
    ModificationRequest:
      title: Запрос с модификацией
      type: object
      properties:
        brand:
          description: Идентификатор марки
          type: integer
        model:
          description: Идентификатор модели
          type: integer
        creationYear:
          description: Год выпуска
          type: integer
        generation:
          description: Идентификатор поколения
          type: integer
        body:
          description: Идентификатор кузова
          type: integer
        doors:
          description: Количество дверей
          type: integer
        gear:
          description: Идентификатор типа КПП
          type: integer
        drive:
          description: Идентификатор типа привода
          type: integer
        engine:
          description: Идентификатор типа двигателя
          type: integer
        volume:
          description: Объем двигателя в литрах
          type: number
        power:
          description: Мощность двигателя в лошадиных силах
          type: integer
        wheel:
          description: Идентификатор руля 1 -- Левый, 2 -- Правый
          type: integer
          enum: [ 1, 2 ]
      required:
        - brand
        - model
        - creationYear
        - body
        - doors
        - gear
        - drive
        - engine
        - power
        - wheel
    ModificationByVinResponse:
      title: Расшифрованная модификация по VIN
      type: object
      properties:
        brand:
          $ref: "#/components/schemas/Brand"
        model:
          $ref: "#/components/schemas/Model"
        creationYear:
          description: Год выпуска
          type: integer
          example: 2008
        generation: # maybe replace with $ref: '#/components/schemas/Generation'
          $ref: '#/components/schemas/GenerationShort'
        body: # maybe include doors and replace with $ref: '#/components/schemas/Body'
          $ref: '#/components/schemas/BodyShort'
        doors:
          description: Количество дверей
          type: integer
          example: 4
        gear:
          $ref: "#/components/schemas/Gear"
        drive:
          $ref: "#/components/schemas/Drive"
        engine: # maybe include power and volume and replace with $ref: '#/components/schemas/Engine'
          $ref: '#/components/schemas/EngineShort'
        volume:
          description: Объем двигателя в литрах
          type: number
          example: 1.4
        power:
          description: Мощность двигателя в лошадиных силах
          type: integer
          example: 75
        wheel:
          $ref: "#/components/schemas/Wheel"
      required:
        - brand
        - model
        - creationYear

security:
  - bearerAuth: [ ]