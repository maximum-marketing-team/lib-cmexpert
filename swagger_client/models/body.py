# coding: utf-8

"""
    Appraisal API: Калькулятор оценки стоимости авто с пробегом

     # Appraisal API **Appraisal API** — API сервиса по оценке стоимости авто с пробегом   # noqa: E501

    OpenAPI spec version: 1.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six


class Body(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'text': 'str',
        'doors': 'int'
    }

    attribute_map = {
        'id': 'id',
        'text': 'text',
        'doors': 'doors'
    }

    def __init__(self, id=None, text=None, doors=None):  # noqa: E501
        """Body - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._text = None
        self._doors = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if text is not None:
            self.text = text
        if doors is not None:
            self.doors = doors

    @property
    def id(self):
        """Gets the id of this Body.  # noqa: E501


        :return: The id of this Body.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Body.


        :param id: The id of this Body.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def text(self):
        """Gets the text of this Body.  # noqa: E501


        :return: The text of this Body.  # noqa: E501
        :rtype: str
        """
        return self._text

    @text.setter
    def text(self, text):
        """Sets the text of this Body.


        :param text: The text of this Body.  # noqa: E501
        :type: str
        """

        self._text = text

    @property
    def doors(self):
        """Gets the doors of this Body.  # noqa: E501


        :return: The doors of this Body.  # noqa: E501
        :rtype: int
        """
        return self._doors

    @doors.setter
    def doors(self, doors):
        """Sets the doors of this Body.


        :param doors: The doors of this Body.  # noqa: E501
        :type: int
        """

        self._doors = doors

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Body, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Body):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
