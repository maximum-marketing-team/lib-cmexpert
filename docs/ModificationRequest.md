# ModificationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | **int** | Идентификатор марки | 
**model** | **int** | Идентификатор модели | 
**creation_year** | **int** | Год выпуска | 
**generation** | **int** | Идентификатор поколения | [optional] 
**body** | **int** | Идентификатор кузова | 
**doors** | **int** | Количество дверей | 
**gear** | **int** | Идентификатор типа КПП | 
**drive** | **int** | Идентификатор типа привода | 
**engine** | **int** | Идентификатор типа двигателя | 
**volume** | **float** | Объем двигателя в литрах | [optional] 
**power** | **int** | Мощность двигателя в лошадиных силах | 
**wheel** | **int** | Идентификатор руля 1 -- Левый, 2 -- Правый | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

