# GeoRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **float** | Широта в градусах | [optional] 
**lon** | **float** | Долгота в градусах | [optional] 
**radius** | **float** | Радиус подбора похожих авто для оценки | [optional] 
**max_extending_radius** | **float** | Максимальный радиус авторасширения | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

