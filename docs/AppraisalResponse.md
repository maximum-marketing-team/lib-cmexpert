# AppraisalResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_categories** | [**PriceCategories**](PriceCategories.md) |  | [optional]
**corrected_price_categories** | [**PriceCategories**](PriceCategories.md) |  | [optional]
**similar_cars_avg_mileage** | **float** |  | [optional]
**complex_estimate** | **int** |  | [optional]
**complex_mds_days** | **float** |  | [optional]
**similar_cars** | **int** |  | [optional]
**corrected_average_price_mileage** | **float** | Считается в зависимости от пробега авто. Если пробег авто не передан или не хватает данных по рынку, то значение будет null | [optional]
**removed_params** | **array** | Параметры, которые не учитываются в расчете

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

