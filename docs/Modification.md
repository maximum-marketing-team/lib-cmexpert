# Modification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | [**Brand**](Brand.md) |  | 
**model** | [**Model**](Model.md) |  | 
**years** | [**ProductionYears**](ProductionYears.md) |  | 
**generation** | [**GenerationShort**](GenerationShort.md) |  | 
**body** | [**BodyShort**](BodyShort.md) |  | 
**doors** | **int** |  | 
**gear** | [**Gear**](Gear.md) |  | 
**drive** | [**Drive**](Drive.md) |  | 
**engine** | [**EngineShort**](EngineShort.md) |  | 
**power** | **int** |  | 
**volume** | **float** |  | 
**wheel** | [**Wheel**](Wheel.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

