# DrivesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drives** | [**list[Drive]**](Drive.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

