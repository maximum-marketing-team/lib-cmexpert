# ModificationByVinResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | [**Brand**](Brand.md) |  | 
**model** | [**Model**](Model.md) |  | 
**creation_year** | **int** | Год выпуска | 
**generation** | [**GenerationShort**](GenerationShort.md) |  | [optional] 
**body** | [**BodyShort**](BodyShort.md) |  | [optional] 
**doors** | **int** | Количество дверей | [optional] 
**gear** | [**Gear**](Gear.md) |  | [optional] 
**drive** | [**Drive**](Drive.md) |  | [optional] 
**engine** | [**EngineShort**](EngineShort.md) |  | [optional] 
**volume** | **float** | Объем двигателя в литрах | [optional] 
**power** | **int** | Мощность двигателя в лошадиных силах | [optional] 
**wheel** | [**Wheel**](Wheel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

