# GenerationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**generations** | [**list[Generation]**](Generation.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

