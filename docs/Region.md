# Region

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**region_id** | **int** |  | 
**name** | **str** |  | 
**type_full_name** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

