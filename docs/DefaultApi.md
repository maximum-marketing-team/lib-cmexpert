# swagger_client.DefaultApi

All URIs are relative to *https://appraisal.api.cm.expert/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_appraisal**](DefaultApi.md#get_appraisal) | **GET** /appraisal | Выполнение оценки легкового автомобиля
[**get_bodies**](DefaultApi.md#get_bodies) | **GET** /autocatalog/bodies | Все кузова доступные для выбранной модели, года и поколения
[**get_brands**](DefaultApi.md#get_brands) | **GET** /autocatalog/brands | Список марок
[**get_creation_years**](DefaultApi.md#get_creation_years) | **GET** /autocatalog/creationYears | Года выпуска производства всех модификаций, выбранной модели
[**get_drives**](DefaultApi.md#get_drives) | **GET** /autocatalog/drives | Все типы приводов, доступные для выбранных модели, года выпуска, поколения, кузова и КПП
[**get_engine_types**](DefaultApi.md#get_engine_types) | **GET** /engineTypes | Список допустимых значений типов двигателей
[**get_engines**](DefaultApi.md#get_engines) | **GET** /autocatalog/engines | Все двигатели (тип, объем и мощность), доступные для выбранных модели, года выпуска, поколения, кузова, КПП и типа привода
[**get_gears**](DefaultApi.md#get_gears) | **GET** /autocatalog/gears | Все типы КПП, доступные для выбранных модели, года, поколения, и кузова
[**get_generations**](DefaultApi.md#get_generations) | **GET** /autocatalog/generations | Все поколения выбранной модели, находящиеся в производстве в выбранный год
[**get_models**](DefaultApi.md#get_models) | **GET** /autocatalog/models | Список моделей
[**get_modification**](DefaultApi.md#get_modification) | **GET** /modification | Получение модификации по вину
[**get_modifications**](DefaultApi.md#get_modifications) | **GET** /autocatalog/modifications | Получение списка модификации
[**get_regions**](DefaultApi.md#get_regions) | **GET** /regions | Список регионов

# **get_appraisal**
> AppraisalResponse get_appraisal(vin=vin, modification=modification, region_id=region_id, geo=geo, mileage=mileage, similar_threshold=similar_threshold, liquidity_region_id=liquidity_region_id)

Выполнение оценки легкового автомобиля

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
vin = 'vin_example' # str | Модификация авто для оценки будет получена расшифровкой VIN. Не все параметры модификации могут быть распознаны по VIN. Для уточненной модификации стоит использовать параметры модификации. Обязательный параметр, но взаимоисключаем модификацией. (optional)
modification = swagger_client.ModificationRequest() # ModificationRequest | Уточненная модификация. Обязательный параметр, но взаимоисключаем VIN. (optional)
region_id = [56] # list[int] | Регион/регионы заданные идентификаторами регионов, которые будут участвовать в подборе похожих авто, для расчета диапазона цен и среднего пробега. Обязательный параметр, но взаимоисключаемы географическими координатами. (optional)
geo = swagger_client.GeoRequest() # GeoRequest | Регион заданный географическими координатами и радиусом, который будет участвовать в подборе похожих авто, для расчета диапазона цен и среднего пробега. Обязательный параметр, но взаимоисключаем идентификатороми регионов. (optional)
mileage = 56 # int | Пробег в км (optional)
similar_threshold = 10 # int | Целевое количество похожих предложений для оценки. Должно быть не меньше 10. (optional) (default to 10)
liquidity_region_id = 56 # int | Идентификатор региона, в котором будет производиться оценка ликвидности. Если параметр осутствует, а регион поиска похожих авто задан одним идентификатором региона, то будет использован он в качестве региона оценки ликвидности. (optional)

try:
    # Выполнение оценки легкового автомобиля
    api_response = api_instance.get_appraisal(vin=vin, modification=modification, region_id=region_id, geo=geo, mileage=mileage, similar_threshold=similar_threshold, liquidity_region_id=liquidity_region_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_appraisal: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vin** | **str**| Модификация авто для оценки будет получена расшифровкой VIN. Не все параметры модификации могут быть распознаны по VIN. Для уточненной модификации стоит использовать параметры модификации. Обязательный параметр, но взаимоисключаем модификацией. | [optional] 
 **modification** | [**ModificationRequest**](.md)| Уточненная модификация. Обязательный параметр, но взаимоисключаем VIN. | [optional] 
 **region_id** | [**list[int]**](int.md)| Регион/регионы заданные идентификаторами регионов, которые будут участвовать в подборе похожих авто, для расчета диапазона цен и среднего пробега. Обязательный параметр, но взаимоисключаемы географическими координатами. | [optional] 
 **geo** | [**GeoRequest**](.md)| Регион заданный географическими координатами и радиусом, который будет участвовать в подборе похожих авто, для расчета диапазона цен и среднего пробега. Обязательный параметр, но взаимоисключаем идентификатороми регионов. | [optional] 
 **mileage** | **int**| Пробег в км | [optional] 
 **similar_threshold** | **int**| Целевое количество похожих предложений для оценки. Должно быть не меньше 10. | [optional] [default to 10]
 **liquidity_region_id** | **int**| Идентификатор региона, в котором будет производиться оценка ликвидности. Если параметр осутствует, а регион поиска похожих авто задан одним идентификатором региона, то будет использован он в качестве региона оценки ликвидности. | [optional] 

### Return type

[**AppraisalResponse**](AppraisalResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_bodies**
> BodiesResponse get_bodies(brand=brand, model=model, creation_year=creation_year, generation=generation)

Все кузова доступные для выбранной модели, года и поколения

Если указаны brand или model, то значения будут выбраны из автокаталога, иначе будут возвращены все возможные значения для свойства кузов без указания количества дверей.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки (optional)
model = 56 # int | Идентификатор модели (optional)
creation_year = 56 # int | Год выпуска (optional)
generation = 56 # int | Идентификатор поколения (optional)

try:
    # Все кузова доступные для выбранной модели, года и поколения
    api_response = api_instance.get_bodies(brand=brand, model=model, creation_year=creation_year, generation=generation)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_bodies: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | [optional] 
 **model** | **int**| Идентификатор модели | [optional] 
 **creation_year** | **int**| Год выпуска | [optional] 
 **generation** | **int**| Идентификатор поколения | [optional] 

### Return type

[**BodiesResponse**](BodiesResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_brands**
> BrandsResponse get_brands()

Список марок

Получение списка марок легковых автомобилей

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    # Список марок
    api_response = api_instance.get_brands()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_brands: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BrandsResponse**](BrandsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_creation_years**
> CreationYearsResponse get_creation_years(brand, model)

Года выпуска производства всех модификаций, выбранной модели

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки
model = 56 # int | Идентификатор модели

try:
    # Года выпуска производства всех модификаций, выбранной модели
    api_response = api_instance.get_creation_years(brand, model)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_creation_years: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | 
 **model** | **int**| Идентификатор модели | 

### Return type

[**CreationYearsResponse**](CreationYearsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_drives**
> DrivesResponse get_drives(brand=brand, model=model, creation_year=creation_year, generation=generation, body=body, doors=doors, gear=gear)

Все типы приводов, доступные для выбранных модели, года выпуска, поколения, кузова и КПП

Если указаны brand или model, то значения будут выбраны из автокаталога, иначе будут возвращены все возможные значения для свойства Тип привода.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки (optional)
model = 56 # int | Идентификатор модели (optional)
creation_year = 56 # int | Год выпуска (optional)
generation = 56 # int | Идентификатор поколения (optional)
body = 56 # int | Идентификатор кузова (optional)
doors = 56 # int | Количество дверей (optional)
gear = 56 # int | Идентификатор типа КПП (optional)

try:
    # Все типы приводов, доступные для выбранных модели, года выпуска, поколения, кузова и КПП
    api_response = api_instance.get_drives(brand=brand, model=model, creation_year=creation_year, generation=generation, body=body, doors=doors, gear=gear)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_drives: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | [optional] 
 **model** | **int**| Идентификатор модели | [optional] 
 **creation_year** | **int**| Год выпуска | [optional] 
 **generation** | **int**| Идентификатор поколения | [optional] 
 **body** | **int**| Идентификатор кузова | [optional] 
 **doors** | **int**| Количество дверей | [optional] 
 **gear** | **int**| Идентификатор типа КПП | [optional] 

### Return type

[**DrivesResponse**](DrivesResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_engine_types**
> EngineTypesResponse get_engine_types()

Список допустимых значений типов двигателей

Метод устарел. Используйте /autocatalog/drives без параметров.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    # Список допустимых значений типов двигателей
    api_response = api_instance.get_engine_types()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_engine_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EngineTypesResponse**](EngineTypesResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_engines**
> EnginesResponse get_engines(brand=brand, model=model, creation_year=creation_year, generation=generation, body=body, doors=doors, gear=gear)

Все двигатели (тип, объем и мощность), доступные для выбранных модели, года выпуска, поколения, кузова, КПП и типа привода

Если указаны brand или model, то значения будут выбраны из автокаталога, иначе будут возвращены все возможные значения для свойства Тип двигателя без указания мощности и объема двигателя.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки (optional)
model = 56 # int | Идентификатор модели (optional)
creation_year = 56 # int | Год выпуска (optional)
generation = 56 # int | Идентификатор поколения (optional)
body = 56 # int | Идентификатор кузова (optional)
doors = 56 # int | Количество дверей (optional)
gear = 56 # int | Идентификатор типа КПП (optional)

try:
    # Все двигатели (тип, объем и мощность), доступные для выбранных модели, года выпуска, поколения, кузова, КПП и типа привода
    api_response = api_instance.get_engines(brand=brand, model=model, creation_year=creation_year, generation=generation, body=body, doors=doors, gear=gear)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_engines: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | [optional] 
 **model** | **int**| Идентификатор модели | [optional] 
 **creation_year** | **int**| Год выпуска | [optional] 
 **generation** | **int**| Идентификатор поколения | [optional] 
 **body** | **int**| Идентификатор кузова | [optional] 
 **doors** | **int**| Количество дверей | [optional] 
 **gear** | **int**| Идентификатор типа КПП | [optional] 

### Return type

[**EnginesResponse**](EnginesResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_gears**
> GearsResponse get_gears(brand=brand, model=model, creation_year=creation_year, generation=generation, body=body, doors=doors)

Все типы КПП, доступные для выбранных модели, года, поколения, и кузова

Если указаны brand или model, то значения будут выбраны из автокаталога, иначе будут возвращены все возможные значения для свойства КПП.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки (optional)
model = 56 # int | Идентификатор модели (optional)
creation_year = 56 # int | Год выпуска (optional)
generation = 56 # int | Идентификатор поколения (optional)
body = 56 # int | Идентификатор кузова (optional)
doors = 56 # int | Количество дверей (optional)

try:
    # Все типы КПП, доступные для выбранных модели, года, поколения, и кузова
    api_response = api_instance.get_gears(brand=brand, model=model, creation_year=creation_year, generation=generation, body=body, doors=doors)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_gears: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | [optional] 
 **model** | **int**| Идентификатор модели | [optional] 
 **creation_year** | **int**| Год выпуска | [optional] 
 **generation** | **int**| Идентификатор поколения | [optional] 
 **body** | **int**| Идентификатор кузова | [optional] 
 **doors** | **int**| Количество дверей | [optional] 

### Return type

[**GearsResponse**](GearsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_generations**
> GenerationsResponse get_generations(brand, model, creation_year=creation_year)

Все поколения выбранной модели, находящиеся в производстве в выбранный год

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки
model = 56 # int | Идентификатор модели
creation_year = 56 # int | Год выпуска (optional)

try:
    # Все поколения выбранной модели, находящиеся в производстве в выбранный год
    api_response = api_instance.get_generations(brand, model, creation_year=creation_year)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_generations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | 
 **model** | **int**| Идентификатор модели | 
 **creation_year** | **int**| Год выпуска | [optional] 

### Return type

[**GenerationsResponse**](GenerationsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_models**
> ModelsResponse get_models(brand)

Список моделей

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки

try:
    # Список моделей
    api_response = api_instance.get_models(brand)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_models: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | 

### Return type

[**ModelsResponse**](ModelsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_modification**
> ModificationByVinResponse get_modification(vin)

Получение модификации по вину

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
vin = 'vin_example' # str | VIN автомобиля

try:
    # Получение модификации по вину
    api_response = api_instance.get_modification(vin)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_modification: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vin** | **str**| VIN автомобиля | 

### Return type

[**ModificationByVinResponse**](ModificationByVinResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_modifications**
> ModificationsResponse get_modifications(brand, model, creation_year, generation=generation)

Получение списка модификации

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
brand = 56 # int | Идентификатор марки
model = 56 # int | Идентификатор модели
creation_year = 56 # int | Год выпуска
generation = 56 # int | Идентификатор поколения (optional)

try:
    # Получение списка модификации
    api_response = api_instance.get_modifications(brand, model, creation_year, generation=generation)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_modifications: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brand** | **int**| Идентификатор марки | 
 **model** | **int**| Идентификатор модели | 
 **creation_year** | **int**| Год выпуска | 
 **generation** | **int**| Идентификатор поколения | [optional] 

### Return type

[**ModificationsResponse**](ModificationsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_regions**
> RegionsResponse get_regions()

Список регионов

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    # Список регионов
    api_response = api_instance.get_regions()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_regions: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**RegionsResponse**](RegionsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

