# PriceCategories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**low** | [**PriceCategory**](PriceCategory.md) |  | 
**middle** | [**PriceCategory**](PriceCategory.md) |  | 
**high** | [**PriceCategory**](PriceCategory.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

