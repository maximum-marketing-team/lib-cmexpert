# coding: utf-8

"""
    Appraisal API: Калькулятор оценки стоимости авто с пробегом

     # Appraisal API **Appraisal API** — API сервиса по оценке стоимости авто с пробегом   # noqa: E501

    OpenAPI spec version: 1.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from models.wheel import Wheel  # noqa: E501
from swagger_client.rest import ApiException


class TestWheel(unittest.TestCase):
    """Wheel unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testWheel(self):
        """Test Wheel"""
        # FIXME: construct object with mandatory attributes with example values
        # model = swagger_client.models.wheel.Wheel()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
